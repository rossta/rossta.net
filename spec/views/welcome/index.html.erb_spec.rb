require 'rails_helper'

RSpec.describe "welcome/index.html.erb", type: :view do
  it "welcomes visitors" do
    render

    expect(rendered).to match(/Welcome/)
  end
end
