namespace :deploy do
  def create_deployment(stack_id, app_id)
    system "aws opsworks create-deployment --stack-id #{stack_id} --app-id #{app_id} --command '{\"Name\":\"deploy\"}'"
  end

  task :production do
    create_deployment ENV.fetch("AWS_STACK_ID_PRODUCTION"), ENV.fetch("AWS_APP_ID_PRODUCTION")
  end
end

desc "Deploy app to OpsWorks production stack"
task :deploy => "deploy:production"
