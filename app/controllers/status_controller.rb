class StatusController < ApplicationController
  skip_before_action :verify_authenticity_token

  def ping
    head :ok
  end
end
