source "https://rubygems.org"

# Bundle edge Rails instead: gem "rails", github: "rails/rails"
gem "rails", "4.2.1"
gem "pg"
gem "sass-rails", "~> 5.0"
gem "uglifier", ">= 1.3.0"
gem "coffee-rails", "~> 4.1.0"
gem "jquery-rails"

# bundle exec rake doc:rails generates the API under doc/api.
gem "sdoc", "~> 0.4.0", group: :doc

# Use ActiveModel has_secure_password
# gem "bcrypt", "~> 3.1.7"

# Use Unicorn as the app server
# gem "unicorn"

group :development, :test do
  # Call "byebug" anywhere in the code to stop execution and get a debugger console
  gem "byebug"

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem "web-console", "~> 2.0"

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
end

gem "unicorn"
gem "redis"
gem "dalli"
gem "sidekiq"
gem "foundation-rails"
gem "bower-rails"
gem "warden"
gem "pundit"
gem "virtus"
gem "rack-protection"
gem "simple_form"
gem "email_validator"
gem "recipient_interceptor"
gem "title"
gem "flutie"
gem "high_voltage"
gem "i18n-tasks"
gem "seed-fu"

group :development, :test do
  gem "factory_girl_rails"
  gem "faker"
  gem "pry-rails"
  gem "pry-rescue"
  gem "pry-byebug"
  gem "awesome_print"
  gem "dotenv-rails"
  gem "rubocop"
end

group :development do
  gem "better_errors"
  gem "guard-bundler"
  gem "guard-rails"
  gem "quiet_assets"
  gem "rails_layout"
  gem "rb-fchange", require: false
  gem "rb-fsevent", require: false
  gem "rb-inotify", require: false
  gem "annotate"
end

group :test do
  gem "rspec-rails"
  gem "capybara"
  gem "shoulda-matchers"
  gem "database_cleaner"
  gem "launchy"
  gem "poltergeist"
  gem "formulaic"
  gem "timecop"
  gem "webmock"
  gem "vcr"
end

gem "newrelic_rpm", ">= 3.7.3"
