require 'rails_helper'

RSpec.describe StatusController, type: :controller do

  describe "GET #ping" do
    it "returns http success" do
      process :ping, 'OPTIONS'
      expect(response).to have_http_status(:success)
    end
  end
end
